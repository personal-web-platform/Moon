# Moon

## Description

Moon is a micro service written with Java and SpringBoot. It acts as the user related requests API for the personal web
platform.

## Technology stack

| Name        | Version |
|-------------|---------|
| Java        | 17      |
| Spring boot | 3.1.0   |