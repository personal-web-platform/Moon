/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.app.config;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class WebConfigurationTest {
    @InjectMocks
    private WebConfiguration webConfiguration;

    @Test
    void testConfigurePathMatch_ShouldSetUseTrailingSlashMatchTrue() {
        // Setup
        PathMatchConfigurer configurer = Mockito.mock(PathMatchConfigurer.class);


        // Run
        webConfiguration.configurePathMatch(configurer);

        // Assert
        verify(configurer).setUseTrailingSlashMatch(true);
    }

    @Test
    void addCorsMappings() {
        // Setup
        CorsRegistry registry = Mockito.mock(CorsRegistry.class);

        // Run
        webConfiguration.addCorsMappings(registry);

        // Assert
        verify(registry, times(1)).addMapping("/**");
    }
}
