/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.app.config;

import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.repositories.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApplicationConfigTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AuthenticationConfiguration authenticationConfiguration;

    @InjectMocks
    private ApplicationConfig applicationConfig;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        applicationConfig = new ApplicationConfig(accountRepository);
    }

    @Test
    void testUserDetailsService() {
        // Setup
        String username = "test@example.com";
        Account userDetails = Account.builder().email("test@example.com").build();
        when(accountRepository.findByEmail(username)).thenReturn(Optional.of(userDetails));

        // Run
        UserDetails result = applicationConfig.userDetailsService().loadUserByUsername(username);

        // Assert
        verify(accountRepository).findByEmail(username);
        assertEquals(userDetails, result);
    }

    @Test
    void testUserDetailsService_UserNotFound() {
        // Setup
        String username = "nonexistent@example.com";
        when(accountRepository.findByEmail(username)).thenReturn(Optional.empty());

        // Run
        assertThrows(UsernameNotFoundException.class, () ->
                applicationConfig.userDetailsService().loadUserByUsername(username));

        // Assert
        verify(accountRepository).findByEmail(username);
    }

    @Test
    void testAuthenticationProvider() {
        // Setup && Run
        AuthenticationProvider authenticationProvider = applicationConfig.authenticationProvider();

        // Assert
        assertNotNull(authenticationProvider);
    }

    @Test
    void testAuthenticationManager() throws Exception {
        // Setup
        when(authenticationConfiguration.getAuthenticationManager()).thenReturn(mock(AuthenticationManager.class));

        // Run
        AuthenticationManager authenticationManager = applicationConfig.authenticationManager(authenticationConfiguration);

        // Assert
        assertNotNull(authenticationManager);
        verify(authenticationConfiguration).getAuthenticationManager();
    }

    @Test
    void testPasswordEncoder() {
        // Setup && Run
        PasswordEncoder passwordEncoder = applicationConfig.passwordEncoder();

        // Assert
        assertNotNull(passwordEncoder);
        assertTrue(passwordEncoder instanceof BCryptPasswordEncoder);
    }
}
