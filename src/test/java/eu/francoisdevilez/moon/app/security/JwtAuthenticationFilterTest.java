/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.app.security;

import eu.francoisdevilez.moon.app.constants.Roles;
import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.entities.Role;
import eu.francoisdevilez.moon.services.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.io.IOException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JwtAuthenticationFilterTest {
    @Mock
    private JwtService jwtService;

    @Mock
    private UserDetailsService userDetailsService;

    @InjectMocks
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        jwtAuthenticationFilter = new JwtAuthenticationFilter(jwtService, userDetailsService);
    }

    @Test
    void doFilterInternal_NullToken() throws ServletException, IOException {
        // Setup
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader(any())).thenReturn(null);

        // Run
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        // Assert
        verify(filterChain, times(1)).doFilter(request, response);
        verify(jwtService, never()).extractEmail(any());
    }

    @Test
    void doFilterInternal_EmptyToken() throws ServletException, IOException {
        // Setup
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader(any())).thenReturn("");

        // Run
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        // Assert
        verify(filterChain, times(1)).doFilter(request, response);
        verify(jwtService, never()).extractEmail(any());
    }

    @Test
    void doFilterInternal_NullEmail() throws ServletException, IOException {
        // Setup
        final String tokenString = "token";
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader(any())).thenReturn(tokenString);
        when(jwtService.extractEmail(any(String.class))).thenReturn(null);

        // Run
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        // Assert
        verify(filterChain, times(1)).doFilter(request, response);
        verify(jwtService, times(1)).extractEmail(tokenString);
        verify(userDetailsService, times(0)).loadUserByUsername(any(String.class));
    }

    @Test
    void doFilterInternal_AlreadyAuthenticated() throws ServletException, IOException {
        // Setup
        final String tokenString = "token";
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        final SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        when(request.getHeader(any())).thenReturn(tokenString);
        when(jwtService.extractEmail(any(String.class))).thenReturn("email@test.com");
        when(SecurityContextHolder.getContext().getAuthentication()).thenReturn(
                new UsernamePasswordAuthenticationToken(null, null)
        );

        // Run
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        // Assert
        verify(filterChain, times(1)).doFilter(request, response);
        verify(jwtService, times(1)).extractEmail(tokenString);
        verify(userDetailsService, times(0)).loadUserByUsername(any(String.class));
    }

    @Test
    void doFilterInternal_Valid() throws ServletException, IOException {
        // Setup
        final String email = "email@test.com";
        final String tokenString = "token";
        final Role role = new Role();
        role.setName(Roles.ROLE_ADMIN);
        final Account account = Account.builder().email("test@example.com").roles(Set.of(role)).build();
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        final SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        when(jwtService.extractEmail(any(String.class))).thenReturn(email);
        when(request.getHeader(any())).thenReturn(tokenString);
        when(SecurityContextHolder.getContext().getAuthentication()).thenReturn(null);
        when(jwtService.isTokenValid(any(), any())).thenReturn(true);
        when(userDetailsService.loadUserByUsername(any())).thenReturn(account);

        // Run
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        // Expect
        verify(filterChain, times(1)).doFilter(request, response);
        verify(jwtService, times(1)).extractEmail(tokenString);
        verify(userDetailsService, times(1)).loadUserByUsername(email);
        verify(securityContext, times(1)).setAuthentication(any(UsernamePasswordAuthenticationToken.class));
    }

    @Test
    void shouldNotFilter_Should_not_filter() {
        // Setup
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequest request2 = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/register");
        when(request2.getRequestURI()).thenReturn("/register/");

        // Run && Expect
        assertTrue(jwtAuthenticationFilter.shouldNotFilter(request));
        assertTrue(jwtAuthenticationFilter.shouldNotFilter(request2));
    }

    @Test
    void shouldNotFilter_Should_filter() {
        // Setup
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/random");

        // Run && Expect
        assertFalse(jwtAuthenticationFilter.shouldNotFilter(request));
    }
}