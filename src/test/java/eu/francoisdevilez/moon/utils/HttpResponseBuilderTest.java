/*
 * Copyright (C) 2024. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.utils;

import eu.francoisdevilez.moon.models.payloads.responses.HttpResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class HttpResponseBuilderTest {

    @Test
    void testBuildSuccessResponse() {
        // Prepare test data
        HttpStatus httpStatus = HttpStatus.OK;
        String data = "Success";

        // Build success response
        ResponseEntity<HttpResponse> response = HttpResponseBuilder.buildSuccessResponse(httpStatus, data);

        // Assert the response properties
        assertEquals(httpStatus, response.getStatusCode());
        HttpResponse responseBody = response.getBody();
        assertNotNull(responseBody);
        assertEquals(data, responseBody.getData());
        assertNull(responseBody.getError());
    }

    @Test
    void testBuildFailureResponse() {
        // Prepare test data
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        String errorMessage = "Internal Server Error";

        // Build failure response
        ResponseEntity<HttpResponse> response = HttpResponseBuilder.buildFailureResponse(httpStatus, new Exception(errorMessage));

        // Assert the response properties
        assertEquals(httpStatus, response.getStatusCode());
        HttpResponse responseBody = response.getBody();
        assertNotNull(responseBody);
        assertNull(responseBody.getData());
        assertEquals(errorMessage, responseBody.getError());
    }
}
