/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.utils;

import eu.francoisdevilez.moon.app.constants.Roles;
import eu.francoisdevilez.moon.models.dto.AccountDTO;
import eu.francoisdevilez.moon.models.dto.RoleDTO;
import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.entities.Role;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilsTest {

    @Test
    void testConvertAccountToAccountDTO() {
        // Setup
        Role role = new Role();
        role.setName(Roles.ROLE_ADMIN);
        Account account = Account.builder().email("test@example.com").roles(Set.of(role)).build();

        // Run
        AccountDTO accountDTO = Utils.convertAccountToAccountDTO(account);

        // Assert
        assertEquals(account.getId(), accountDTO.getId());
        assertEquals(account.getEmail(), accountDTO.getEmail());
        assertEquals(account.getPassword(), accountDTO.getPassword());
        assertEquals(account.getCreatedAt(), accountDTO.getCreatedAt());
        assertEquals(account.getFirstName(), accountDTO.getFirstName());
        assertEquals(account.getLastName(), accountDTO.getLastName());
    }

    @Test
    void testConvertRoleToRoleDTO() {
        // Setup
        Role role = new Role();
        role.setId(new UUID(1, 1));
        role.setName(Roles.ROLE_USER);

        // Run
        RoleDTO roleDTO = Utils.convertRoleToRoleDTO(role);

        // Assert
        assertEquals(role.getId(), roleDTO.getId());
        assertEquals(role.getName().name(), roleDTO.getName());
    }

    @Test
    void testConvertRolesToRoleDTOs() {
        // Setup
        Set<Role> roles = new HashSet<>();
        Role role1 = new Role();
        role1.setId(new UUID(1, 1));
        role1.setName(Roles.ROLE_USER);
        Role role2 = new Role();
        role2.setId(new UUID(1, 1));
        role2.setName(Roles.ROLE_ADMIN);
        roles.add(role1);
        roles.add(role2);

        // Run
        Set<RoleDTO> roleDTOs = Utils.convertRolesToRoleDTOs(roles);

        // Assert
        assertEquals(roles.size(), roleDTOs.size());
    }

    @Test
    void testConvertAccountDTOToAccount() {
        // Setup
        AccountDTO accountDTO = AccountDTO.builder().email("test@example.com").password("password").createdAt(Instant.now()).firstName("John").lastName("Doe").build();
        Set<RoleDTO> roleDTOs = new HashSet<>(
                List.of(
                        RoleDTO.builder().id(new UUID(1, 1)).name(Roles.ROLE_USER.name()).build(),
                        RoleDTO.builder().id(new UUID(1, 1)).name(Roles.ROLE_ADMIN.name()).build()
                ));
        accountDTO.setRoles(roleDTOs);

        // Run
        Account account = Utils.convertAccountDTOToAccount(accountDTO);

        // Assert
        assertEquals(accountDTO.getEmail(), account.getEmail());
        assertEquals(accountDTO.getPassword(), account.getPassword());
        assertEquals(accountDTO.getCreatedAt(), account.getCreatedAt());
        assertEquals(accountDTO.getFirstName(), account.getFirstName());
        assertEquals(accountDTO.getLastName(), account.getLastName());

        Set<Role> roles = account.getRoles();
        assertEquals(roleDTOs.size(), roles.size());
    }
}
