/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.services;

import eu.francoisdevilez.moon.app.constants.Constants;
import eu.francoisdevilez.moon.models.entities.Token;
import eu.francoisdevilez.moon.models.repositories.TokenRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LogoutServiceTest {

    @Mock
    private TokenRepository tokenRepository;

    @InjectMocks
    private LogoutService logoutService;

    @Test
    void testLogout_ValidToken() {
        // Setup
        Token token = new Token();
        token.setStringToken("valid-token");
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(Constants.TOKEN_HEADER, "valid-token");
        MockHttpServletResponse response = new MockHttpServletResponse();
        Authentication authentication = mock(Authentication.class);
        when(tokenRepository.findByStringToken("valid-token")).thenReturn(Optional.of(token));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Run
        logoutService.logout(request, response, authentication);

        // Assert
        assertTrue(token.getExpired());
        assertTrue(token.getRevoked());
        verify(tokenRepository).save(token);
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testLogout_InvalidToken() {
        // Setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(Constants.TOKEN_HEADER, "invalid-token");
        MockHttpServletResponse response = new MockHttpServletResponse();
        Authentication authentication = mock(Authentication.class);
        when(tokenRepository.findByStringToken("invalid-token")).thenReturn(Optional.empty());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Run
        logoutService.logout(request, response, authentication);

        // Assert
        verify(tokenRepository).findByStringToken("invalid-token");
        verify(tokenRepository, never()).save(any());
        assertNotNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testLogout_NoToken() {
        // Setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        Authentication authentication = mock(Authentication.class);

        // Run
        logoutService.logout(request, response, authentication);

        // Assert
        verify(tokenRepository, never()).findByStringToken(any());
        verify(tokenRepository, never()).save(any());
    }
}
