/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.services;

import eu.francoisdevilez.moon.app.constants.Roles;
import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.entities.Role;
import eu.francoisdevilez.moon.models.entities.Token;
import eu.francoisdevilez.moon.models.payloads.requests.LoginRequest;
import eu.francoisdevilez.moon.models.payloads.requests.PatchRoleRequest;
import eu.francoisdevilez.moon.models.payloads.requests.RegisterRequest;
import eu.francoisdevilez.moon.models.payloads.responses.HttpResponse;
import eu.francoisdevilez.moon.models.repositories.AccountRepository;
import eu.francoisdevilez.moon.models.repositories.RoleRepository;
import eu.francoisdevilez.moon.models.repositories.TokenRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {
    @Mock
    private AccountRepository accountRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private TokenRepository tokenRepository;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtService jwtService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AccountService accountService;

    @Test
    void testInsertNewAccount_AccountAlreadyExists() {
        // Setup
        RegisterRequest registerRequest = RegisterRequest.builder().email("test@example.com").build();
        Role role = new Role();
        role.setName(Roles.ROLE_USER);
        when(accountRepository.findByEmail(registerRequest.getEmail())).thenReturn(Optional.of(new Account()));
        when(roleRepository.findByName(Roles.ROLE_USER)).thenReturn(Optional.of(role));

        // Run
        ResponseEntity<HttpResponse> response = accountService.insertNewAccount(registerRequest);

        // Assert
        verify(accountRepository).findByEmail(registerRequest.getEmail());
        assertEquals("Given account already exists", response.getBody().getError());
    }

    @Test
    void testInsertNewAccount_RoleNotFound() {
        // Setup
        RegisterRequest registerRequest = RegisterRequest.builder()
                .email("test@example.com")
                .firstName("John")
                .lastName("Doe")
                .password("password")
                .build();

        when(roleRepository.findByName(Roles.ROLE_USER)).thenReturn(Optional.empty());

        // Run
        ResponseEntity<HttpResponse> response = accountService.insertNewAccount(registerRequest);

        // Assert
        verify(roleRepository).findByName(Roles.ROLE_USER);
        assertEquals("Given role - ROLE_USER does not exists", response.getBody().getError());
    }

    @Test
    void testInsertNewAccount_Successful() {
        // Setup
        RegisterRequest registerRequest = RegisterRequest.builder()
                .email("test@example.com")
                .firstName("John")
                .lastName("Doe")
                .password("password")
                .build();
        Role role = new Role();
        role.setName(Roles.ROLE_USER);
        Account tmp = Account.builder()
                .email("email@test.com")
                .password("password")
                .firstName("")
                .lastName("")
                .roles(Set.of())
                .tokens(Set.of())
                .createdAt(Instant.now())
                .build();

        when(accountRepository.findByEmail(registerRequest.getEmail())).thenReturn(Optional.empty());
        when(roleRepository.findByName(Roles.ROLE_USER)).thenReturn(Optional.of(role));
        when(accountRepository.save(any(Account.class))).thenReturn(tmp);

        // Run
        ResponseEntity<HttpResponse> response = accountService.insertNewAccount(registerRequest);

        // Assert
        verify(accountRepository).findByEmail(registerRequest.getEmail());
        verify(roleRepository).findByName(Roles.ROLE_USER);
        verify(accountRepository).save(any(Account.class));
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    void testInsertNewAccount_Exception() {
        // Setup
        RegisterRequest registerRequest = RegisterRequest.builder()
                .email("test@example.com")
                .firstName("John")
                .lastName("Doe")
                .password("password")
                .build();
        Role role = new Role();
        role.setName(Roles.ROLE_USER);

        when(accountRepository.findByEmail(registerRequest.getEmail())).thenReturn(Optional.empty());
        when(roleRepository.findByName(Roles.ROLE_USER)).thenReturn(Optional.of(role));
        Account tmpAccount = Account.builder().email("email@test.com").password("password").firstName("").lastName("").createdAt(null).roles(Set.of()).tokens(Set.of()).build();
        when(accountRepository.save(any(Account.class))).thenReturn(tmpAccount);

        // Run
        ResponseEntity<HttpResponse> response = accountService.insertNewAccount(registerRequest);

        // Assert
        assertEquals("Something went wrong while creating new account", response.getBody().getError());
    }

    @Test
    void testLogin_NoAccountFound() {
        // Setup
        String email = "test@example.com";
        LoginRequest loginRequest = LoginRequest.builder().email(email).password("password").build();

        when(accountRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.empty());

        // Run
        ResponseEntity<HttpResponse> response = accountService.login(loginRequest);

        // Assert
        assertEquals("Given account - test@example.com does not exists", response.getBody().getError());
    }

    @Test
    void testLogin_Successful() {
        // Setup
        String email = "test@example.com";
        LoginRequest loginRequest = LoginRequest.builder().email(email).password("password").build();

        Account account = Account.builder().email("email@test.com").password("password").firstName("").lastName("").roles(Set.of()).tokens(Set.of()).build();
        Token token = Token.builder().id(UUID.randomUUID()).stringToken("token").revoked(false).expired(false).account(account).createdAt(OffsetDateTime.now()).expireAt(OffsetDateTime.now().plusHours(1)).updatedAt(OffsetDateTime.now()).build();

        when(accountRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.of(account));
        when(jwtService.generateToken(any(Account.class))).thenReturn(token);
        when(tokenRepository.findAllByAccountAndRevokedFalseAndExpiredFalse(any(Account.class))).thenReturn(new ArrayList<>());
        when(tokenRepository.save(any(Token.class))).thenReturn(token);
        when(jwtService.extractExpiration(any())).thenReturn(new Date(token.getExpireAt().toInstant().toEpochMilli()));

        // Run
        ResponseEntity<HttpResponse> response = accountService.login(loginRequest);

        // Assert
        verify(accountRepository).findByEmail(loginRequest.getEmail());
        verify(jwtService).generateToken(account);
        verify(tokenRepository).findAllByAccountAndRevokedFalseAndExpiredFalse(account);
        verify(tokenRepository).save(any(Token.class));
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testLogin_Exception() {
        // Setup
        String email = "test@example.com";
        LoginRequest loginRequest = LoginRequest.builder().email(email).password("password").build();
        when(authenticationManager.authenticate(any())).thenThrow(mock(AuthenticationException.class));

        // Run
        ResponseEntity<HttpResponse> response = accountService.login(loginRequest);

        // Assert
        assertNull(response.getBody().getError());
    }

    @Test
    void testLogin_revokeAllTokens() {
        // Setup
        String email = "test@example.com";
        LoginRequest loginRequest = LoginRequest.builder().email(email).password("password").build();
        Role role = new Role();
        role.setName(Roles.ROLE_USER);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        Account account = Account.builder().email("email@test.com").password("password").firstName("").lastName("").roles(Set.of()).tokens(Set.of()).build();
        Token token = Token.builder()
                .stringToken("")
                .revoked(false)
                .expired(false)
                .stringToken("token")
                .createdAt(OffsetDateTime.now())
                .updatedAt(OffsetDateTime.now())
                .expireAt(OffsetDateTime.now().plusHours(1))
                .build();
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token);

        when(accountRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.of(account));
        when(jwtService.generateToken(any(Account.class))).thenReturn(token);
        when(tokenRepository.save(any(Token.class))).thenReturn(token);
        when(jwtService.extractExpiration(any())).thenReturn(new Date(token.getExpireAt().toInstant().toEpochMilli()));
        when(tokenRepository.findAllByAccountAndRevokedFalseAndExpiredFalse(any())).thenReturn(tokenList);

        // Run
        ResponseEntity<HttpResponse> response = accountService.login(loginRequest);

        // Assert
        verify(accountRepository).findByEmail(loginRequest.getEmail());
        verify(jwtService).generateToken(account);
        verify(tokenRepository).findAllByAccountAndRevokedFalseAndExpiredFalse(account);
        verify(tokenRepository).save(any(Token.class));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(token.getExpired());
        assertTrue(token.getRevoked());
    }

    @Test
    void testVerifyToken_TokenExpired() {
        // Setup
        String jwt = "test-jwt";
        Token token = new Token();
        token.setExpired(true);
        when(tokenRepository.findByStringToken(jwt)).thenReturn(Optional.of(token));

        // Run
        ResponseEntity<HttpResponse> response = accountService.verifyToken(jwt);

        // Assert
        verify(tokenRepository).findByStringToken(jwt);
        assertEquals("Given token is either expired or revoked", response.getBody().getError());
    }

    @Test
    void testVerifyToken_AccountNotFound() {
        // Setup
        String jwt = "test-jwt";
        Token token = new Token();
        token.setExpired(false);
        token.setRevoked(false);
        when(tokenRepository.findByStringToken(jwt)).thenReturn(Optional.of(token));
        when(accountRepository.findByEmail(any(String.class))).thenReturn(Optional.empty());
        when(jwtService.extractEmail(any())).thenReturn("email@test.com");

        // Run
        ResponseEntity<HttpResponse> response = accountService.verifyToken(jwt);

        // Assert
        verify(accountRepository).findByEmail(any(String.class));
        verify(tokenRepository).findByStringToken(jwt);
        assertEquals("Given account - email@test.com does not exists", response.getBody().getError());
    }

    @Test
    void testVerifyToken_NoToken() {
        // Setup
        when(tokenRepository.findByStringToken(anyString())).thenReturn(Optional.empty());

        // Run
        ResponseEntity<HttpResponse> response = accountService.verifyToken("");

        // Assert
        assertEquals("No token found", response.getBody().getError());
    }

    @Test
    void testVerifyToken_Successful() {
        // Setup
        String jwt = "test-jwt";
        Token token = new Token();
        token.setExpired(false);
        token.setRevoked(false);
        token.setStringToken(jwt);
        Account account = Account.builder().email("email@test.com").password("password").firstName("").lastName("").roles(Set.of()).tokens(Set.of()).build();
        token.setAccount(account);
        when(tokenRepository.findByStringToken(jwt)).thenReturn(Optional.of(token));
        when(accountRepository.findByEmail(any())).thenReturn(Optional.of(account));
        when(jwtService.extractEmail(any())).thenReturn(account.getEmail());

        // Run
        ResponseEntity<HttpResponse> response = accountService.verifyToken(jwt);

        // Assert
        verify(tokenRepository).findByStringToken(jwt);
        verify(accountRepository).findByEmail(account.getEmail());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testPatchAccountRole_AccountNotFound() {
        // Setup
        PatchRoleRequest patchRoleRequest = PatchRoleRequest.builder().email("test@example.com").newRole(Roles.ROLE_ADMIN).build();
        when(accountRepository.findByEmail(patchRoleRequest.getEmail())).thenReturn(Optional.empty());
        when(tokenRepository.findByStringToken(anyString())).thenReturn(Optional.of(
                Token.builder().stringToken("token").build()
        ));

        // Run
        ResponseEntity<HttpResponse> response = accountService.patchAccountRole(patchRoleRequest, "token");

        // Assert
        verify(accountRepository).findByEmail(patchRoleRequest.getEmail());
        assertEquals("Given account - test@example.com does not exists", response.getBody().getError());
    }

    @Test
    void testPatchAccountRole_RoleAlreadyAssigned() {
        // Setup
        PatchRoleRequest patchRoleRequest = PatchRoleRequest.builder().email("test@example.com").newRole(Roles.ROLE_ADMIN).build();
        Role role = new Role();
        role.setName(Roles.ROLE_ADMIN);
        Account account = Account.builder().email("test@example.com").roles(Set.of(role)).build();
        when(accountRepository.findByEmail(patchRoleRequest.getEmail())).thenReturn(Optional.of(account));
        when(roleRepository.findByName(patchRoleRequest.getNewRole())).thenReturn(Optional.of(role));
        when(tokenRepository.findByStringToken(anyString())).thenReturn(Optional.of(new Token()));

        // Run
        ResponseEntity<HttpResponse> response = accountService.patchAccountRole(patchRoleRequest, "token");

        // Assert
        verify(accountRepository).findByEmail(patchRoleRequest.getEmail());
        assertEquals("Given role ROLE_ADMIN is already assigned to user - test@example.com", response.getBody().getError());
    }

    @Test
    void testPatchAccountRole_NoToken() {
        // Setup
        PatchRoleRequest patchRoleRequest = PatchRoleRequest.builder().email("test@example.com").newRole(Roles.ROLE_ADMIN).build();
        when(tokenRepository.findByStringToken(anyString())).thenReturn(Optional.empty());

        // Run
        ResponseEntity<HttpResponse> response = accountService.patchAccountRole(patchRoleRequest, "token");

        // Assert
        assertEquals("No token found", response.getBody().getError());
    }

    @Test
    void testPatchAccountRole_RoleNotFound() {
        // Setup
        PatchRoleRequest patchRoleRequest = PatchRoleRequest.builder().email("test@example.com").newRole(Roles.ROLE_ADMIN).build();
        Role role = new Role();
        role.setName(Roles.ROLE_USER);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        Account account = Account.builder().email("test@example.com").roles(roles).build();
        when(accountRepository.findByEmail(patchRoleRequest.getEmail())).thenReturn(Optional.of(account));
        when(tokenRepository.findByStringToken(anyString())).thenReturn(Optional.of(new Token()));
        when(roleRepository.findByName(patchRoleRequest.getNewRole())).thenReturn(Optional.empty());

        // Run
        ResponseEntity<HttpResponse> response = accountService.patchAccountRole(patchRoleRequest, "token");

        // Assert
        assertEquals("Given role - ROLE_ADMIN does not exists", response.getBody().getError());
    }

    @Test
    void testPatchAccountRole_Successful() {
        // Setup
        PatchRoleRequest patchRoleRequest = PatchRoleRequest.builder().email("test@example.com").newRole(Roles.ROLE_ADMIN).build();
        Role role = new Role();
        role.setName(Roles.ROLE_USER);
        Role role2 = new Role();
        role2.setName(Roles.ROLE_ADMIN);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        Account account = Account.builder().email("test@example.com").roles(roles).build();
        when(accountRepository.findByEmail(patchRoleRequest.getEmail())).thenReturn(Optional.of(account));
        when(roleRepository.findByName(patchRoleRequest.getNewRole())).thenReturn(Optional.of(role2));
        when(accountRepository.save(any(Account.class))).thenReturn(new Account());
        when(tokenRepository.findByStringToken(anyString())).thenReturn(Optional.of(new Token()));

        // Run
        ResponseEntity<HttpResponse> response = accountService.patchAccountRole(patchRoleRequest, "");

        // Assert
        verify(accountRepository).findByEmail(patchRoleRequest.getEmail());
        verify(roleRepository).findByName(patchRoleRequest.getNewRole());
        verify(accountRepository).save(any(Account.class));
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}