/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.services;

import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.entities.Token;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class JwtServiceTest {
    private final String secretKey = "AD1A55FDAF25C2FA9729668E5E64D04B6779F9FC5271E48321969B2F26B5E388";
    private final String expiracyTime = "3600000";
    private JwtService jwtService;
    @Mock
    private Account account;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        jwtService = new JwtService();
        ReflectionTestUtils.setField(jwtService, "secretKey", secretKey);
        ReflectionTestUtils.setField(jwtService, "tokenExpiracyTime", expiracyTime);
    }

    @Test
    void testExtractEmail() {
        // Setup
        String jwt = generateJwt("test@example.com", new Date(), new Date(System.currentTimeMillis() + 10000));

        // Run
        String extractedEmail = jwtService.extractEmail(jwt);

        // Assert
        assertEquals("test@example.com", extractedEmail);
    }

    @Test
    void testExtractIssueAt() {
        // Setup
        Date issuedAt = new Date();
        String jwt = generateJwt("test@example.com", issuedAt, new Date(System.currentTimeMillis() + 10000));

        // Run
        Date extractedIssueAt = jwtService.extractIssueAt(jwt);

        // Assert
        assertEquals(1, issuedAt.compareTo(extractedIssueAt));
    }

    @Test
    void testExtractExpiration() {
        // Setup
        Date expiration = new Date(System.currentTimeMillis() + 10000);
        String jwt = generateJwt("test@example.com", new Date(), expiration);

        // Run
        Date extractedExpiration = jwtService.extractExpiration(jwt);

        // Assert
        assertEquals(1, expiration.compareTo(extractedExpiration));
    }

    @Test
    void testExtractClaim() {
        // Setup
        HashMap<String, String> tmp = new HashMap<>();
        tmp.put("claim1", "value1");
        Claims claims = new DefaultClaims(tmp);
        String jwt = generateJwtWithClaims("test@example.com", new Date(), new Date(System.currentTimeMillis() + 10000), claims);

        // Run
        String extractedClaim = jwtService.extractClaim(jwt, c -> c.get("claim1", String.class));

        // Assert
        assertEquals("value1", extractedClaim);
    }

    @Test
    void testGenerateToken_WithExtraClaims() {
        // Setup
        Map<String, Object> extraClaims = new HashMap<>();
        extraClaims.put("claim1", "value1");
        when(account.getUsername()).thenReturn("test@example.com");

        // Run
        Token token = jwtService.generateToken(extraClaims, account);

        // Assert
        assertNotNull(token);
        assertEquals("test@example.com", token.getAccount().getUsername());
    }

    @Test
    void testGenerateToken_WithoutExtraClaims() {
        // Setup
        when(account.getUsername()).thenReturn("test@example.com");

        // Run
        Token token = jwtService.generateToken(account);

        // Assert
        assertNotNull(token);
        assertEquals("test@example.com", token.getAccount().getUsername());
    }

    @Test
    void testIsTokenValid_ValidTokenAndUserDetails() {
        // Setup
        String jwt = generateJwt("test@example.com", new Date(), new Date(System.currentTimeMillis() + 10000));
        Account account = Account.builder().email("test@example.com").build();

        // Run
        boolean isValid = jwtService.isTokenValid(jwt, account);

        // Assert
        assertTrue(isValid);
    }

    @Test
    void testIsTokenValid_InvalidToken() {
        // Setup
        String jwt = generateJwt("test@example.com", new Date(), new Date());
        Account account = Account.builder().email("test@example.com").build();

        // Run && Assert
        assertThrows(ExpiredJwtException.class, () -> jwtService.isTokenValid(jwt, account));
    }

    @Test
    void testIsTokenValid_ExpiredToken() {
        // Setup
        Date expiration = new Date(System.currentTimeMillis() - 3600000); // Expired 1 hour ago
        String jwt = generateJwt("test@example.com", new Date(), expiration);
        Account account = Account.builder().email("test@example.com").build();

        // Run && Assert
        assertThrows(ExpiredJwtException.class, () -> jwtService.isTokenValid(jwt, account));
    }

    @Test
    void testIsTokenValid_InvalidTokenFormat() {
        // Setup
        String jwt = "invalid.token.format";
        Account account = Account.builder().email("test@example.com").build();

        // Run && Assert
        assertThrows(MalformedJwtException.class, () ->
                jwtService.isTokenValid(jwt, account));
    }

    @Test
    void testIsTokenValid_ExpiredJwtException() {
        // Setup
        String jwt = generateJwt("test@example.com", new Date(), new Date());

        // Run && Assert
        assertThrows(ExpiredJwtException.class, () ->
                jwtService.isTokenValid(jwt, null));
    }

    private String generateJwt(String email, Date issuedAt, Date expiration) {
        return Jwts
                .builder()
                .subject(email)
                .issuedAt(issuedAt)
                .expiration(expiration)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
                .compact();
    }

    private String generateJwtWithClaims(String email, Date issuedAt, Date expiration, Claims claims) {
        return Jwts
                .builder()
                .subject(email)
                .claims(claims)
                .issuedAt(issuedAt)
                .expiration(expiration)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
                .compact();
    }
}
