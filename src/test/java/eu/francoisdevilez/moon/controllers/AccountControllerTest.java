/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.controllers;

import eu.francoisdevilez.moon.app.constants.Constants;
import eu.francoisdevilez.moon.models.payloads.requests.LoginRequest;
import eu.francoisdevilez.moon.models.payloads.requests.PatchRoleRequest;
import eu.francoisdevilez.moon.models.payloads.requests.RegisterRequest;
import eu.francoisdevilez.moon.models.payloads.responses.HttpResponse;
import eu.francoisdevilez.moon.services.AccountService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {
    @Mock
    private AccountService accountService;

    @Mock
    private HttpServletRequest request = new MockHttpServletRequest();

    @InjectMocks
    private AccountController accountController;

    @Test
    void testRegisterNewAccount_Success() {
        // Setup
        RegisterRequest registerRequest = RegisterRequest.builder().build();
        when(accountService.insertNewAccount(registerRequest)).thenReturn(ResponseEntity.ok().build());

        // Run
        ResponseEntity<HttpResponse> response = accountController.registerNewAccount(request, registerRequest);

        // Assert
        verify(accountService).insertNewAccount(registerRequest);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testLogin_Success() {
        // Setup
        LoginRequest loginRequest = LoginRequest.builder().build();
        when(accountService.login(loginRequest)).thenReturn(ResponseEntity.ok().build());

        // Run
        ResponseEntity<HttpResponse> response = accountController.login(request, loginRequest);

        // Assert
        verify(accountService).login(loginRequest);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testVerifyAccount_Success() {
        // Setup
        String token = "valid-token";
        Map<String, String> headers = Collections.singletonMap(Constants.TOKEN_HEADER, token);
        when(accountService.verifyToken(token)).thenReturn(ResponseEntity.ok().build());

        // Run
        ResponseEntity<HttpResponse> response = accountController.verifyAccount(request, headers);

        // Assert
        verify(accountService).verifyToken(token);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testPatchRole_Success() {
        // Setup
        Map<String, String> headers = new HashMap<>();
        headers.put("token", "token");
        PatchRoleRequest patchRoleRequest = PatchRoleRequest.builder().build();
        when(accountService.patchAccountRole(patchRoleRequest, "token")).thenReturn(ResponseEntity.ok().build());

        // Run
        ResponseEntity<HttpResponse> response = accountController.patchRole(request, patchRoleRequest, headers);

        // Assert
        verify(accountService).patchAccountRole(patchRoleRequest, "token");
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
