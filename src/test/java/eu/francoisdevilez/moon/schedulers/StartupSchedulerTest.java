/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.schedulers;

import eu.francoisdevilez.moon.services.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class StartupSchedulerTest {
    @Mock
    private AccountService accountService;

    @InjectMocks
    private StartupScheduler startupScheduler;

    @Test
    void testStartup_Successful() {
        // Setup
        ReflectionTestUtils.setField(startupScheduler, "adminAccount", "test@gmail.com");
        ReflectionTestUtils.setField(startupScheduler, "adminPassword", "password");

        // Run
        this.startupScheduler.startup();

        // Assert
        verify(accountService, times(1)).insertAdminAccount(any());
    }
}