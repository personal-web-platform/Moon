CREATE TABLE accounts
(
    id           UUID PRIMARY KEY      DEFAULT gen_random_uuid(),
    email        VARCHAR(255) NOT NULL UNIQUE,
    password     VARCHAR(255) NOT NULL,
    first_name   VARCHAR(255) NOT NULL,
    last_name    VARCHAR(255) NOT NULL,
    created_at   TIMESTAMPTZ           DEFAULT NOW(),
    updated_at   TIMESTAMPTZ           DEFAULT NOW()
);

CREATE TABLE roles
(
    id         UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name       VARCHAR(255) NOT NULL,
    created_at TIMESTAMPTZ      DEFAULT NOW(),
    updated_at TIMESTAMPTZ      DEFAULT NOW()
);

CREATE TABLE tokens
(
    id           UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    string_token VARCHAR(255) NOT NULL,
    revoked      BOOLEAN      NOT NULL,
    expired      BOOLEAN      NOT NULL,
    account_id   UUID REFERENCES accounts (id),
    expire_at    TIMESTAMPTZ  NOT NULL,
    created_at   TIMESTAMPTZ      DEFAULT NOW(),
    updated_at   TIMESTAMPTZ      DEFAULT NOW()
);

CREATE TABLE account_roles
(
    account_id UUID REFERENCES accounts (id) ON DELETE CASCADE,
    role_id    UUID REFERENCES roles (id) ON DELETE CASCADE,
    PRIMARY KEY (account_id, role_id)
);

CREATE TRIGGER update_account_updated_at
    BEFORE UPDATE
    ON moon.accounts
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();

CREATE TRIGGER update_roles_updated_at
    BEFORE UPDATE
    ON moon.roles
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();

CREATE TRIGGER update_tokens_updated_at
    BEFORE UPDATE
    ON moon.tokens
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();

