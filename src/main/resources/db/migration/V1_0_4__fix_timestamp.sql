/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

UPDATE moon.tokens
SET created_at = NOW()
WHERE created_at IS NULL;
UPDATE moon.tokens
SET expire_at = NOW()
WHERE expire_at IS NULL;
UPDATE moon.tokens
SET updated_at = NOW()
WHERE updated_at IS NULL;

ALTER TABLE moon.tokens
    ALTER COLUMN created_at SET NOT NULL,
    ALTER COLUMN expire_at SET NOT NULL,
    ALTER COLUMN updated_at SET NOT NULL;
