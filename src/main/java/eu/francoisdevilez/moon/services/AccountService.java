/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.services;

import eu.francoisdevilez.moon.app.constants.Roles;
import eu.francoisdevilez.moon.app.errors.exceptions.*;
import eu.francoisdevilez.moon.models.dto.AccountDTO;
import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.entities.Role;
import eu.francoisdevilez.moon.models.entities.Token;
import eu.francoisdevilez.moon.models.payloads.requests.LoginRequest;
import eu.francoisdevilez.moon.models.payloads.requests.PatchRoleRequest;
import eu.francoisdevilez.moon.models.payloads.requests.RegisterRequest;
import eu.francoisdevilez.moon.models.payloads.responses.AuthenticationResponse;
import eu.francoisdevilez.moon.models.payloads.responses.HttpResponse;
import eu.francoisdevilez.moon.models.repositories.AccountRepository;
import eu.francoisdevilez.moon.models.repositories.RoleRepository;
import eu.francoisdevilez.moon.models.repositories.TokenRepository;
import eu.francoisdevilez.moon.utils.HttpResponseBuilder;
import eu.francoisdevilez.moon.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static eu.francoisdevilez.moon.utils.HttpResponseBuilder.buildFailureResponse;

@Service
@AllArgsConstructor
@Slf4j
public class AccountService {
    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    public ResponseEntity<HttpResponse> insertNewAccount(RegisterRequest registerRequest) {
        final Role role = roleRepository.findByName(Roles.ROLE_USER).orElse(null);
        if (role == null) {
            Exception exception = new RoleNotFoundException(Roles.ROLE_USER.toString());
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.INTERNAL_SERVER_ERROR, exception);
        }
        return this.insertNewAccount(registerRequest, role);
    }

    public void insertAdminAccount(RegisterRequest registerRequest) {
        this.insertNewAccount(registerRequest, roleRepository.findByName(Roles.ROLE_ADMIN).orElse(null));
    }

    private ResponseEntity<HttpResponse> insertNewAccount(RegisterRequest registerRequest, Role role) {
        if (accountRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
            Exception exception = new AccountAlreadyExistException();
            log.error(exception.getMessage());
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.CONFLICT, exception);
        }

        log.info("Roles to be set on new account - {}", role.getName());
        AccountDTO accountDTO = AccountDTO.builder()
                .email(registerRequest.getEmail())
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .roles(Stream.of(Utils.convertRoleToRoleDTO(role)).collect(Collectors.toSet()))
                .build();

        final Account accountToInsert = Utils.convertAccountDTOToAccount(accountDTO);
        log.info("Inserting new account - {}", accountToInsert);
        Account insertedAccount = accountRepository.save(accountToInsert);

        final AccountDTO insertedAccountDTO = Utils.convertAccountToAccountDTO(insertedAccount);
        if (insertedAccountDTO.getCreatedAt() != null) {
            return HttpResponseBuilder.buildSuccessResponse(
                    HttpStatus.CREATED,
                    insertedAccountDTO
            );
        }

        Exception exception = new CustomException("Something went wrong while creating new account", null);
        log.error(exception.getMessage());
        return buildFailureResponse(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    public ResponseEntity<HttpResponse> login(LoginRequest loginRequest) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getEmail(),
                            loginRequest.getPassword()
                    )
            );
        } catch (AuthenticationException exception) {
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        log.info("Successfully authenticating user with email - {}", loginRequest.getEmail());
        Account account = accountRepository.findByEmail(loginRequest.getEmail()).orElse(null);
        if (account == null) {
            Exception exception = new AccountNotFoundException(loginRequest.getEmail());
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        Token jwt = jwtService.generateToken(account);
        revokeAllAccountToken(account);
        tokenRepository.save(jwt);
        return HttpResponseBuilder.buildSuccessResponse(
                HttpStatus.OK,
                AuthenticationResponse.builder()
                        .token(jwt.getStringToken())
                        .iat(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS))
                        .exp(jwtService.extractExpiration(jwt.getStringToken()).toInstant().truncatedTo(ChronoUnit.SECONDS))
                        .role(Utils.convertAccountToAccountDTO(account).getRoles())
                        .build()
        );
    }

    public ResponseEntity<HttpResponse> verifyToken(String jwt) {
        Token token = tokenRepository.findByStringToken(jwt).orElse(null);
        if (token == null) {
            TokenNotFoundException exception = new TokenNotFoundException();
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        if (token.getExpired() || token.getRevoked()) {
            TokenExpiredException exception = new TokenExpiredException();
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        Account account = accountRepository.findByEmail(jwtService.extractEmail(jwt)).orElse(null);
        if (account == null) {
            AccountNotFoundException exception = new AccountNotFoundException(jwtService.extractEmail(jwt));
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        AccountDTO accountDTO = Utils.convertAccountToAccountDTO(account);
        return HttpResponseBuilder.buildSuccessResponse(
                HttpStatus.OK,
                accountDTO
        );
    }

    public ResponseEntity<HttpResponse> patchAccountRole(PatchRoleRequest patchRoleRequest, String jwt) {
        Token token = tokenRepository.findByStringToken(jwt).orElse(null);
        if (token == null) {
            Exception exception = new TokenNotFoundException();
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        Account account = accountRepository.findByEmail(patchRoleRequest.getEmail()).orElse(null);
        if (account == null) {
            Exception exception = new AccountNotFoundException(patchRoleRequest.getEmail());
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }
        Role newRole = roleRepository.findByName(patchRoleRequest.getNewRole()).orElse(null);
        if (newRole == null) {
            Exception exception = new RoleNotFoundException(patchRoleRequest.getNewRole().name());
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }

        Set<Role> tmp = account.getRoles().stream().filter(item -> item.getName() == newRole.getName()).collect(Collectors.toSet());
        if (!tmp.isEmpty()) {
            Exception exception = new RoleAlreadyAssignedException(newRole.getName().toString(), account.getEmail());
            log.error(exception.getMessage());
            return buildFailureResponse(HttpStatus.BAD_REQUEST, exception);
        }

        account.getRoles().add(newRole);
        accountRepository.save(account);
        return HttpResponseBuilder.buildSuccessResponse(
                HttpStatus.OK,
                Utils.convertAccountToAccountDTO(account)
        );
    }

    private void revokeAllAccountToken(Account account) {
        List<Token> tokens = tokenRepository.findAllByAccountAndRevokedFalseAndExpiredFalse(account);
        if (tokens.isEmpty()) {
            return;
        }
        tokens.forEach(token -> {
            token.setRevoked(true);
            token.setExpired(true);
        });
        tokenRepository.saveAll(tokens);
    }
}
