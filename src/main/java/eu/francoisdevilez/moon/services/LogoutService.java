/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.services;

import eu.francoisdevilez.moon.app.constants.Constants;
import eu.francoisdevilez.moon.app.errors.exceptions.TokenNotFoundException;
import eu.francoisdevilez.moon.models.entities.Token;
import eu.francoisdevilez.moon.models.repositories.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class LogoutService implements LogoutHandler {
    private final TokenRepository tokenRepository;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws TokenNotFoundException {
        final String jwt = request.getHeader(Constants.TOKEN_HEADER);
        if (jwt == null || jwt.isEmpty()) {
            Exception exception = new TokenNotFoundException();
            log.error(exception.getMessage());
        } else {
            Token storedToken = tokenRepository.findByStringToken(jwt).orElse(null);
            if (storedToken != null) {
                storedToken.setExpired(true);
                storedToken.setRevoked(true);
                tokenRepository.save(storedToken);
                SecurityContextHolder.clearContext();
            } else {
                TokenNotFoundException exception = new TokenNotFoundException();
                log.error(exception.getMessage());
            }
        }
    }
}
