/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.utils;

import eu.francoisdevilez.moon.app.constants.Roles;
import eu.francoisdevilez.moon.models.dto.AccountDTO;
import eu.francoisdevilez.moon.models.dto.RoleDTO;
import eu.francoisdevilez.moon.models.entities.Account;
import eu.francoisdevilez.moon.models.entities.Role;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public interface Utils {
    static AccountDTO convertAccountToAccountDTO(Account account) {
        return AccountDTO.builder()
                .id(account.getId())
                .email(account.getEmail())
                .password(account.getPassword())
                .createdAt(account.getCreatedAt())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .roles(account.getRoles().stream().map(Utils::convertRoleToRoleDTO).collect(Collectors.toSet()))
                .build();
    }

    static RoleDTO convertRoleToRoleDTO(Role role) {
        return RoleDTO.builder().id(role.getId()).name(role.getName().name()).build();
    }

    static Set<RoleDTO> convertRolesToRoleDTOs(Set<Role> roles) {
        return roles.stream().map(Utils::convertRoleToRoleDTO).collect(Collectors.toSet());
    }

    static Account convertAccountDTOToAccount(AccountDTO accountDTO) {
        final Set<RoleDTO> roleDTOs = accountDTO.getRoles();
        final Set<Role> roles = new HashSet<>();
        for (RoleDTO roleDTO : roleDTOs) {
            Role role = new Role();
            role.setId(roleDTO.getId());
            if (roleDTO.getName().equals(Roles.ROLE_USER.name())) {
                role.setName(Roles.ROLE_USER);
            }
            if (roleDTO.getName().equals(Roles.ROLE_ADMIN.name())) {
                role.setName(Roles.ROLE_ADMIN);
            }
            roles.add(role);
        }
        return Account.builder()
                .email(accountDTO.getEmail())
                .password(accountDTO.getPassword())
                .firstName(accountDTO.getFirstName())
                .lastName(accountDTO.getLastName())
                .roles(roles)
                .createdAt(accountDTO.getCreatedAt())
                .updatedAt(accountDTO.getUpdatedAt())
                .build();
    }
}
