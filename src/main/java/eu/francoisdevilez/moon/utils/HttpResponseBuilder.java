/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.utils;

import eu.francoisdevilez.moon.models.payloads.responses.HttpResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface HttpResponseBuilder {
    static <T> ResponseEntity<HttpResponse> buildSuccessResponse(HttpStatus httpStatus, T data) {
        return new ResponseEntity<>(HttpResponse.builder().data(data).error(null).build(), httpStatus);
    }

    static ResponseEntity<HttpResponse> buildFailureResponse(HttpStatus httpStatus, Exception exception) {
        return new ResponseEntity<>(HttpResponse.builder().data(null).error(exception.getMessage()).build(), httpStatus);
    }
}
