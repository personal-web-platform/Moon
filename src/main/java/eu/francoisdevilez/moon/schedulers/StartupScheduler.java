/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.schedulers;

import eu.francoisdevilez.moon.models.payloads.requests.RegisterRequest;
import eu.francoisdevilez.moon.services.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StartupScheduler {
    private final AccountService accountService;

    @Value("${admin.account}")
    private String adminAccount;

    @Value("${admin.password}")
    private String adminPassword;

    @EventListener(org.springframework.boot.context.event.ApplicationReadyEvent.class)
    public void startup() {
        log.info("Running startup script");
        this.accountService.insertAdminAccount(RegisterRequest.builder()
                .email(adminAccount)
                .password(adminPassword)
                .firstName("")
                .lastName("")
                .build());
    }
}
