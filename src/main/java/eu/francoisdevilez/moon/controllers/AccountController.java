/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.controllers;

import eu.francoisdevilez.moon.app.constants.Constants;
import eu.francoisdevilez.moon.models.payloads.requests.LoginRequest;
import eu.francoisdevilez.moon.models.payloads.requests.PatchRoleRequest;
import eu.francoisdevilez.moon.models.payloads.requests.RegisterRequest;
import eu.francoisdevilez.moon.models.payloads.responses.HttpResponse;
import eu.francoisdevilez.moon.services.AccountService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/account")
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;

    @PostMapping({"/register", "/register/"})
    public ResponseEntity<HttpResponse> registerNewAccount(HttpServletRequest request, @RequestBody RegisterRequest registerRequest) {
        return accountService.insertNewAccount(registerRequest);
    }

    @PostMapping({"/login", "/login/"})
    public ResponseEntity<HttpResponse> login(HttpServletRequest request, @RequestBody LoginRequest loginRequest) {
        return accountService.login(loginRequest);
    }

    @PostMapping({"/verify", "/verify/"})
    public ResponseEntity<HttpResponse> verifyAccount(HttpServletRequest request, @RequestHeader Map<String, String> headers) {
        return accountService.verifyToken(headers.get(Constants.TOKEN_HEADER));
    }

    @PostMapping({"/patchRole", "/patchRole/"})
    public ResponseEntity<HttpResponse> patchRole(HttpServletRequest request, @RequestBody PatchRoleRequest patchRoleRequest, @RequestHeader Map<String, String> headers) {
        return accountService.patchAccountRole(patchRoleRequest, headers.get(Constants.TOKEN_HEADER));
    }
}
