/*
 * Copyright (C) 2024. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.models.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Builder
public class AccountDTO {
    private UUID id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Instant createdAt;
    private Instant updatedAt;
    private Set<RoleDTO> roles;
    private Set<TokenDTO> tokens;

    @JsonValue
    public Object convertToJSON() {
        HashMap<String, Object> object = new HashMap<>();
        object.put("email", this.email);
        object.put("firstName", this.firstName);
        object.put("lastName", this.lastName);
        object.put("role", this.roles);
        return object;
    }
}
