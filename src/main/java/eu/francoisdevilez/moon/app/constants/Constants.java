/*
 * Copyright (C) 2025. FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.francoisdevilez.moon.app.constants;

public interface Constants {
    String TOKEN_HEADER = "token";

    // endpoints
    // Don't add trailing slash here, will be added automatically
    String REGISTER_ROUTE = "/account/register";
    String LOGIN_ROUTE = "/account/login";
    String VERIFY_ROUTE = "/account/verify";
    String LOGOUT_ROUTE = "/account/logout";
    String PATCH_ROLE_ROUTE = "/account/patchRole";
    String ACTUATOR_ROUTE = "/actuator/**";
}
